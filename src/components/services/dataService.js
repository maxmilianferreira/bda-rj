import config from './../constants'
import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
export default class DataService {

  buscarCensoPorTipo (tipo, periodo, codigoCenso, campoPesquisa) {
    let ultimaVisao = ''
    let campo = ''
    if (Number(periodo)) {
      ultimaVisao = '(Periodo=' + periodo + ')'
    } else {
      ultimaVisao = '(1=1)'
    }
    if (campoPesquisa !== '') {
      campo = ',' + campoPesquisa
    }
    let params = {
      where: '(1=1)%20AND%20' + ultimaVisao,
      returnGeometry: false,
      spatialRel: 'esriSpatialRelIntersects',
      outFields: 'OBJECTID,Periodo,' + codigoCenso + campo,
      orderByFields: 'OBJECTID%20ASC,Periodo,' + codigoCenso + campo,
      outSR: 102100,
      resultOffset: 0,
      resultRecordCount: 50
    }

    return new Promise((resolve, reject) => {
      Vue.http.get(config.constants.URL_API_BASE + tipo + '/query?f=pjson&where=' + params.where + '&returnGeometry=' + params.returnGeometry + '&spatialRel=' + params.spatialRel + '&outFields=' + params.outFields + '&orderByFields=' + params.orderByFields + '&outSR=' + params.outSR + '&resultOffset=' + params.resultOffset + '&resultRecordCount=' + params.resultRecordCount).then(response => {
        resolve(response.body)
      }, error => {
        reject(error)
      })
    })
  }
}
