import Vue from 'vue'
import Router from 'vue-router'
import Censo from '@/components/Censo'
import CensoEscolar from '@/components/CensoEscolar'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BDA-RJ-Censo',
      component: Censo
    },
    {
      path: '/censo',
      name: 'BDA-RJ-Censo',
      component: Censo
    },
    {
      path: '/censo-escolar',
      name: 'BDA-RJ-Censo',
      component: CensoEscolar
    }
  ]
})
