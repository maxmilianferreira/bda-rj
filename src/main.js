window.jQuery = require('jquery')
require('bootstrap')
require('terraformer')
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import MenuBda from './components/layout/Menu'
import Header from './components/layout/Header'
import Mapa from './components/Mapa'
import Pesquisa from './components/Pesquisa'
import Footer from './components/layout/Footer'
import Table from './components/pesquisa/Table'
import Pie from './components/pesquisa/Pie'
import Line from './components/pesquisa/Line'
import Column from './components/pesquisa/Column'
import Area from './components/pesquisa/Area'
import 'bootstrap/dist/css/bootstrap.css'
import VueConstants from 'vue-constants'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import VueCharts from 'vue-charts'
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
import VuePaginate from 'vue-paginate'
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
Vue.use(VueCharts)
Vue.use(VuePaginate)
Vue.use(VueLodash, lodash)
Vue.component('icon', Icon)
Vue.component('menu-bda', MenuBda)
Vue.component('menu-header', Header)
Vue.component('mapa', Mapa)
Vue.component('bda-footer', Footer)
Vue.component('bda-table', Table)
Vue.component('bda-line-chart', Line)
Vue.component('bda-area-chart', Area)
Vue.component('pesquisa', Pesquisa)
Vue.component('bda-pie-chart', Pie)
Vue.component('bda-column-chart', Column)
Vue.use(VueChartkick, { Chartkick })
Vue.use(VueConstants)
Vue.use(VueResource)
Vue.config.productionTip = true
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App, Icon }
})
