        // GeoJSON layer
        // let featureLayer
        // let simplePointSym = new SimplePointSymbol()
        // let simpleLineSym = new SimpleLineSymbol('solid', new Color([50, 50, 50, 1]), 1)
        // let simplePolygonSym = new SimpleFillSymbol('solid', simpleLineSym, new Color([255, 50, 50, 0.5]))
        // getGeoJSON()
        // function getGeoJSON () {
        //   let requestHandle = esriRequest({
        //     url: 'https://armazemdados.img.com.br/server/rest/services/BDA/BDA_PILOTO/MapServer/dynamicLayer/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&objectIds=1&outFields=*&outSR=102100&layer=%7B%22source%22%3A%7B%22type%22%3A%22mapLayer%22%2C%22mapLayerId%22%3A0%7D%7D',
        //     handleAs: 'json'
        //   })
        //   requestHandle.then(addGeoJSONLayer, errorLoadingGeoJSON)
        // }
        let featureLayer2 = new FeatureLayer('https://armazemdados.img.com.br/server/rest/services/BDA/BDA_PILOTO/MapServer/0', {
          mode: FeatureLayer.MODE_ONDEMAND,
          outFields: ['*']
        })
        map.add(featureLayer2)
        setTimeout(function () {
          let elems = document.getElementsByClassName('esri-component')
          for (let k = elems.length - 1; k >= 0; k--) {
            let parent = elems[k].parentNode
            parent.removeChild(elems[k])
          }
        }, 1)
        // GeoJSON to ArcGIS geometry type
        // function getEsriGeometryType (geometryGeoJson) {
        //   let type
        //   switch (geometryGeoJson) {
        //     case 'Point':
        //       type = 'esriGeometryPoint'
        //       break
        //     case 'MultiPoint':
        //       type = 'esriGeometryMultipoint'
        //       break
        //     case 'LineString':
        //       type = 'esriGeometryPolyline'
        //       break
        //     case 'Polygon':
        //       type = 'esriGeometryPolygon'
        //       break
        //     case 'MultiPolygon':
        //       type = 'esriGeometryPolygon'
        //       break
        //   }
        //   return type
        // }
        // function getEsriSymbol (geometryType) {
        //   let sym
        //   switch (geometryType) {
        //     case 'esriGeometryPoint':
        //       sym = simplePointSym
        //       break
        //     case 'esriGeometryMultipoint':
        //       sym = simplePointSym
        //       break
        //     case 'esriGeometryPolyline':
        //       sym = simpleLineSym
        //       break
        //     case 'esriGeometryPolygon':
        //       sym = simplePolygonSym
        //       break
        //   }
        //   return sym
        // }
        // function createFeatureCollection (geometryType) {
        //   // TODO - need to pull definition from GeoJSON attributes!
        //   let layerDefinition = {
        //     'geometryType': geometryType,
        //     'objectIdField': 'ObjectID',
        //     'fields': [{
        //       'name': 'ObjectID',
        //       'alias': 'ObjectID',
        //       'type': 'esriFieldTypeOID'
        //     }, {
        //       'name': 'type',
        //       'alias': 'Type',
        //       'type': 'esriFieldTypeString'
        //     }]
        //   }
        //   let featureCollection = {
        //     layerDefinition: layerDefinition,
        //     featureSet: {
        //       features: [],
        //       geometryType: geometryType
        //     }
        //   }
        //   return featureCollection
        // }
        // function createFeature (arcgis, sym) {
        //   let shape = new Graphic(arcgis).setSymbol(sym)
        //   // Do other things with shape...
        //   return shape
        // }
        // // Use Terraformer to convert geojson to arcgis json
        // function createFeatureSet (geojson, geometryType) {
        //   let featureSet = []
        //   // convert the geojson object to a arcgis json representation
        //   let primitive = new Terraformer.FeatureCollection(geojson)
        //   let arcgis = Terraformer.ArcGIS.convert(primitive)
        //   // NOTE: Assumes same geojson geometry type for entire file!
        //   let sym = getEsriSymbol(geometryType)
        //   for (let i = 0; i < arcgis.length; i++) {
        //     featureSet.push(createFeature(arcgis[i], sym))
        //   }
        //   return featureSet
        // }
        // // Create a feature layer for the GeoJSON
        // function addGeoJSONLayer (geojson) {
        //   if (!geojson.features.length) {
        //     return
        //   }
        //   removeGeoJSONLayer()
        //   // Get geometry type - assume same geometry for entire file!
        //   let esriGeometryType = getEsriGeometryType(geojson.features[0].geometry.type)
        //   // Create an skeleton collection and popup definition
        //   let featureCollection = createFeatureCollection(esriGeometryType)
        //   let infoTemplate = new InfoTemplate('GeoJSON Data', '{*}')
        //   // Create feature layer
        //   featureLayer = new FeatureLayer(featureCollection, {
        //     mode: FeatureLayer.MODE_SNAPSHOT,
        //     outFields: ['*'],
        //     infoTemplate: infoTemplate
        //   })
        //   // Add it to the map
        //   map.addLayer(featureLayer)
        //   // Add graphics to the featurelayer
        //   let featureSet = createFeatureSet(geojson, esriGeometryType)
        //   featureLayer.applyEdits(featureSet, null, null)
        // }
        // function removeGeoJSONLayer () {
        //   if (featureLayer) {
        //     map.removeLayer(featureLayer)
        //     map.infoWindow.hide()
        //   }
        // }
        // Error
        // function errorLoadingGeoJSON (e) {
        //   console.log('Error loading GeoJSON. ' + e)
        // }